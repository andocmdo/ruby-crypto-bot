# Main container script
require 'csv'
require 'date'
require_relative 'CryptoBot.rb'
require_relative 'DummyDataSource.rb'

bot = CryptoBot.new(data_source: DummyDataSource.new)

puts bot.total_value

# Main event loop
# The bot could also be triggered by a webhook, or crontab?
run = true
iters = 0
while run
  bot.tick
  if (iters += 1) > 3
    run = false
  end
  sleep 3
end
