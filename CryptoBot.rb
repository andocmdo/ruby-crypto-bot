require 'json'

class CryptoBot
  attr_accessor :cash, :coins, :event_log, :state

  def initialize(
    cash: 10000.00,
    coins: 0.0,
    warmup_ticks: 7,
    data_preload_ticks: 0,
    data_source: nil)

    @cash = cash
    @coins = coins
    @warmup_ticks = warmup_ticks
    @data_source = data_source
    @event_log = []
    # depending on whether we have pre-loaded data we may have to "warmup"
    if data_preload_ticks < @warmup_ticks
      @state = "warmup"
    else
      @state = "looking_to_buy"
    end
  end

  # this is called from outside controller to trigger the bot to run through
  # it's logic loop once
  def tick
    puts "tick"
    # when we first startup we need a "warmup" period. Or need to preload past data
    # for the bot to start with. After it's been running it will have historical data to use
    case @state
    when "warmup"
      #just get data
      get_quotes
      @event_log << {action: "warmup", total_value: total_value}
    when "looking_to_buy"
      #looking to make a deal :)
      puts "looking to buy"
    when "looking_to_sell"
      # get out of this market!
      puts "looking to sell"
    when "waiting"  #??
      # don't do anything
      puts "waiting"
    else
      # throw an error???
    end
    puts @event_log
  end

  def get_quotes
    get_sell_quote
    get_buy_quote
  end

  def get_buy_quote
    #stuff
    @data_source.get_buy_quote
  end

  def get_sell_quote
    #stuff
    @data_source.get_sell_quote
  end

  def total_value
    @cash + (@coins * @data_source.last_sell_quote[:price])
  end

  def to_s
    puts "----------"
    puts "CryptoBot State"
    puts "cash: #{@cash}"
    puts "coins: #{@coins}"
    puts "total value (at last price): #{total_value}"
    puts "----------"
  end
end
