# The data_source is the wrapper to any API or database to get prices
# It will handle rate limiting (although the bot should still be efficient in it's calls for data)
# The DataSource should have the following methods:
# get_quote - should return at minimum a price, better yet, an ask/bid price (buy/sell) (hash)
# - attempt to refresh if possible
# last_quote - minimize network, return the last available quote (hash)
# - don't refresh
# quotes - the full array of quotes (maybe param to say how many we need)
#
class DummyDataSource
  attr_accessor :quotes

  def initialize
    @prng = Random.new
    @quotes = Array.new
    @quotes << {datetime: 1632185357, price: 123.45}
    @quotes << {datetime: 1632185358, price: 123.46}
  end

  # try to refresh from network if possible
  def get_buy_quote
    some_amount = @prng.rand(3.14)
    next_datetime = @quotes.last[:datetime] + 1
    next_price = @quotes.last[:price] + some_amount
    {datetime: next_datetime, price: next_price}
  end

  # get the last cached quote
  def last_buy_quote
    @quotes.last
  end

  # try to refresh from network if possible
  def get_sell_quote
    some_amount = @prng.rand(3.14)
    next_datetime = @quotes.last[:datetime] + 1
    next_price = @quotes.last[:price] + some_amount
    {datetime: next_datetime, price: next_price}
  end

  # get the last cached quote
  def last_sell_quote
    @quotes.last
  end


end
