require 'net/http'
require 'json'
require 'uri'

class CoinbasePriceGetter
  def initialize(api_delay: 2, spacing: 30, interval: 900, error_file: )
    # delay between two api calls, like for getting buy/sell pairs in seconds
    @api_delay = api_delay
    # delay between getting each coin listed in the file in seconds
    @spacing = spacing
    # overal update interval (minutes * 60)
    @interval = interval
    @error_file = error_file
  end

  def write_line_to_file(file, line)
    File.open(file, 'a') do |f|
      f.puts line
    end
  end

  # just makes a pipe delimited
  def create_log_line(*args)
    ts = Time.now.to_i
    line = "#{ts}"
    args.each do |s|
      line = line + "|#{s}"
    end
    line
  end

  def log_response(filename, action, resp)
     if resp.is_a?(Net::HTTPSuccess)
       status = "OK"
       json = JSON.parse(resp.body)
       line = create_log_line(status, action, json["data"]["amount"], resp.body)
       write_line_to_file(filename, line)
     else
       status = "ERROR"
       line = create_log_line(status, action, "", resp.body)
       write_line_to_file(@error_file, line) # also put errors in error file
       write_line_to_file(filename, line)
     end
  end

  def http_get(url)
    uri = URI(url)
    Net::HTTP.get_response(uri)
  end

  def call_api_buy(coin)
     url = "https://api.coinbase.com/v2/prices/#{coin}-USD/buy"
     http_get(url)
  end

  def call_api_sell(coin)
     url = "https://api.coinbase.com/v2/prices/#{coin}-USD/sell"
     http_get(url)
  end

  def get_price_data(coin)
     log_response("#{coin}.pdv", "BUY", call_api_buy(coin))
     puts "api_delay #{@api_delay}"
     sleep(@api_delay)
     log_response("#{coin}.pdv", "SELL", call_api_sell(coin))
  end

  def run
    @run = true
    @iter = 0
    while @run
      @iter += 1
      File.readlines('coins.txt').each do |line|
        coin = line.strip
        if !coin.empty?
          get_price_data(coin)
          puts "spacing #{@spacing}"
          sleep(@spacing)
        end
      end
      puts "interval #{@interval}"
      sleep(@interval)
    end
  end
end

price_script = CoinbasePriceGetter.new(error_file: "errors.pdv")
price_script.run
